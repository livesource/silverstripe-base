<?php

global $project;
$project = 'app';

// Database
global $database;
require_once("conf/ConfigureFromEnv.php");

if (SS_DATABASE_CLASS == 'SQLiteDatabase') {
    global $databaseConfig;
    $databaseConfig = array(
        "type" => 'SQLiteDatabase',
        "server" => 'none',
        "username" => 'none',
        "password" => 'none',
        "database" => 'db.sqlite',
        "path" => BASE_PATH . "/db.sqlite",
    );
}

// Redirects
if (Director::isLive() && isset($_SERVER['HTTP_HOST']) && strpos($_SERVER['HTTP_HOST'], 'www') === 0) {
    $destURL = str_replace('www.', '', Director::absoluteURL($_SERVER['REQUEST_URI']));
    $response = new SS_HTTPResponse("<h1>Your browser is not accepting header redirects</h1><p>Please <a href=\"$destURL\">click here</a>", 301);
    HTTP::add_cache_headers($response);
    $response->addHeader('Location', $destURL);
    $response->output();
    die;
}

// Requirements
Requirements::set_backend(new BetterRequirementsBackend());

// Search
//FulltextSearchable::enable();

// Images
//Config::inst()->update('ImageOptimiserService', 'binDirectory', BASE_PATH . "/$project/bin");
//Image::set_backend("OptimisedGDBackend");

// Error and Notice logging
Email::setAdminEmail("shea@livesource.co.nz");
SS_Log::add_writer(new SS_LogFileWriter(BASE_PATH . '/app/logs/info.log'), SS_Log::INFO);
SS_Log::add_writer(new SS_LogFileWriter(BASE_PATH . '/app/logs/error.log'), SS_Log::NOTICE, '<=');

if (Director::isLive()) {
    $writer = new SS_LogEmailWriter(Config::inst()->get('Email', 'admin_email'));
    $writer->setFormatter(new SS_LogErrorEmailFormatter());
    SS_Log::add_writer($writer, SS_Log::WARN, '<=');
}

if (!Director::isDev()) {
    $writer = new SS_LogEmailWriter(Email::getAdminEmail());
    $formatter = new LogErrorEmailFormatterCondensed();
    $writer->setFormatter($formatter);
    SS_Log::add_writer(
        $writer,
        SS_Log::WARN,
        '<='
    );
}

// CMS
CMSMenu::remove_menu_item('ReportAdmin');
i18n::set_locale('en_NZ');
HtmlEditorConfig::get('cms')->setOptions(array(
    "skin" => "default",
    "style_formats" => array(
        // array(
        // 	"title" => "Lead",
        // 	"selector" => "p",
        // 	"classes" => "leadin"
        // ),
        // array(
        // 	"title" => "Button",
        // 	"selector" => "a",
        // 	"classes" => "btn"
        // ),
        array(
            "title" => "Image: float left",
            "selector" => "img",
            "classes" => "left"
        ),
        array(
            "title" => "Image: float right",
            "selector" => "img",
            "classes" => "right"
        ),
        array(
            "title" => "Image: alone left",
            "selector" => "img",
            "classes" => "leftAlone"
        ),
        array(
            "title" => "Image: alone center",
            "selector" => "img",
            "classes" => "center"
        )
    )
));
