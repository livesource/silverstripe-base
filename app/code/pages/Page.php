<?php
class Page extends BasePage
{

    private static $hide_ancestor = 'BasePage';

    private static $db = array(
    );

    private static $has_one = array(
    );

    private static $has_image = false;
}
class Page_Controller extends BasePage_Controller
{

    public function init()
    {
        parent::init();
        
        if (Controller::curr()->class == 'AdminSecurity') {
            return;
        }

        Requirements::block(THIRDPARTY_DIR . '/jquery/jquery.js');
        if (defined('RESPONSIVE_IMAGES_DIR')) {
            Requirements::block(RESPONSIVE_IMAGES_DIR.'/javascript/picturefill/picturefill.js');
        }
    }
}
