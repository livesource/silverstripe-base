<?php

class HomePage extends Page
{
    
    public function onBeforeWrite()
    {
        parent::onBeforeWrite();
        // force url segment to be home
        $this->URLSegment = 'home';
    }
}

class HomePage_Controller extends Page_Controller
{
}
