<?php

class BasePage extends SiteTree
{
    private static $db = array(
        "MetaTitle" => "Varchar(255)"
    );

    private static $has_one = array(
        'Image' => 'Image'
    );

    private static $image_default = 'default-image.jpg';

    private static $has_image = false;

    /**
     * Limit the number of pages of this type that can be created
     * @var int
     **/
    private static $limit_pages;


    public function getCMSFields()
    {
        $fields = parent::getCMSFields();
        
        $fields->dataFieldByName('MenuTitle')->setRightTitle('If your page title needs to be different to your navigation label, edit the page title field first, then update this navigation label field.');
        
        $metaComposite = $fields->fieldByName('Root.Main.Metadata');
        $metaComposite->insertBefore(
            TextField::create('MetaTitle')
                ->setAttribute('placeholder', $this->Title),
            'MetaDescription'
        );

        if ($this->config()->get('has_image')) {
            $fields->addFieldToTab(
                "Root.Images",
                $imageField = UploadField::create('Image', 'Image')
                    ->setAllowedFileCategories('image')
                    ->setAllowedMaxFileNumber(1)
                    ->setFolderName($this->config()->get('image_folder'))
            );
            $imageField->getValidator()->setAllowedMaxFileSize(2097152);
        }

        return $fields;
    }

    /**
     * AFMetaTags function. Overridden to allow custom meta description
     * 
     * @return string $tags
     */
    public function MetaTags($includeTitle = true)
    {
        $tags = parent::MetaTags($includeTitle);

        if (!$this->MetaDescription) {
            $tags .= "<meta name=\"description\" content=\"" . Convert::raw2att($this->getMetaDesc()) . "\" />\n";
        }

        return $tags;
    }


    /**
     * The title for this page, used in meta tag
     * @return string
     **/
    public function PageTitle()
    {
        $title = $this->MetaTitle ? $this->MetaTitle : $this->Title;
        return $title . ' | ' . $this->getSiteConfig()->Title;
    }


    /**
     * Fallback to summary or content for meta description tag
     * @return string
     **/
    public function getMetaDesc()
    {
        return $this->MetaDescription ? $this->MetaDescription : Convert::raw2att($this->SummaryOrContent()->Summary(30));
    }


    /**
     * Image or default for opengraph
     * @return String
     **/
    public function getOGImage()
    {
        $image = $this->ImageOrDefault(true);
        if ($image && $image->exists()) {
            return $image->CroppedFocusedImage(600, 400)->getAbsoluteURL();
        }
    }


    /**
     * desription for opengraph
     * @return String
     **/
    public function getOGDescription()
    {
        return $this->getMetaDesc();
    }


    /**
     * Summary or content
     * @return DBField
     **/
    public function SummaryOrContent()
    {
        return $this->hasField('Summary') ? $this->dbObject('Summary') : $this->dbObject('Content');
    }


    /**
     * Gets the image for this page or falls back to default
     * @param boolean $fallbackInLive
     * @return Image
     **/
    public function ImageOrDefault($fallbackInLive = false)
    {
        if (!$fallbackInLive) {
            if (Director::isLive()) {
                return $this->Image();
            }
        }
        $image = $this->Image()->exists() ? $this->Image() : $this->DefaultImage();
        return $image;
    }


    /**
     * Gets the default image
     * @return Image
     **/
    public function DefaultImage()
    {
        return Image::get()->filter('Name', $this->config()->image_default)->first();
    }


    /**
     * Get sibling pages of this page
     * @return DataList
     **/
    public function getSiblings()
    {
        return SiteTree::get()->filter('ParentID', $this->ParentID)->exclude('ID', $this->ID);
    }


    /**
     * Get the next page
     * @return SiteTree
     **/
    public function Next()
    {
        return $this->getSiblings()->where("Sort > $this->Sort")->first();
    }


    /**
     * Get the previous page
     * @return SiteTree
     **/
    public function Prev()
    {
        return $this->getSiblings()->where("Sort < $this->Sort")->last();
    }

    public function canCreate($members = null)
    {
        $limit = $this->config()->get('limit_pages');
        if ($limit) {
            return DataObject::get($this->owner->ClassName)->count() < $limit;
        }
        return true;
    }
}

class BasePage_Controller extends ContentController
{

    private static $allowed_actions = array(
        'dismiss_browser_rejection',
        'SearchForm'
    );

    
    /**
     * Stores file extension prefix for js and css files
     * will either be empty or ".min"
     * @var string
     **/
    protected $min = '';


    public function init()
    {
        parent::init();
        $theme = SSViewer::get_theme_folder();
        Requirements::set_combined_files_folder($theme . '/combined');
        $this->min = !Director::isDev() ? '.min' : '';
    }

    /**
     * Used for conditional partial caching
     * @return Boolean
     **/
    public function isLive()
    {
        return Director::isLive();
    }

    
    /**
     * function SearchForm()
     * @return custom SearchForm object
     */
    public function SearchForm()
    {
        $searchText = isset($this->Query) ?: $this->Query;
        $fields = FieldList::create(TextField::create("Search", "", $searchText)
            ->setAttribute('placeholder', 'Search')
            ->setAttribute('autocomplete', 'off')
        );
        $actions = FieldList::create(FormAction::create('results', 'Go'));
        $form = SearchForm::create($this, "SearchForm", $fields, $actions);
        $form->setFormAction(Controller::join_links(BASE_URL, 'search', 'results'));
        return $form;
    }


    /**
     * A css class to apply to the <body>
     * @return string
     */
    public function CSSClass()
    {
        return 'type_' . strtolower($this->ClassName) . ' stage_' . strtolower(Versioned::current_stage());
    }
    

    public function LoadPartialLink()
    {
        return "PageAPI_Controller/loadpartial/" . $this->data()->ID;
    }


    /**
     * Controller action to dismiss the browser rejection notice
     */
    public function dismiss_browser_rejection($request)
    {
        Session::set('BrowserRejectionDismissed', 1);
        return $this->redirectBack();
    }


    /**
     * Used in template to check if browser notice has been dismissed
     */
    public function BrowserRejectionDismissed()
    {
        return Session::get('BrowserRejectionDismissed');
    }
}

class PageAPI_Controller extends Controller
{
    private static $allowed_actions = array(
        'loadpartial'
    );


    /**
     * Load dynamic content in a partial template via ajax so the rest of the page can be static cached
     * @return string
     **/
    public function loadpartial($request)
    {
        if ($page = SiteTree::get()->byID((int)$request->param('ID'))) {
            $template = $request->getVar('template');
            if ($template && SSViewer::hasTemplate(array($template))) {
                return $page->renderWith($template);
            }
        }
    }
}
