<?php
/**
 * Backs up the files and databases hosted in this cPanel account. Optionally sends backup to remote FTP server
 * Based on http://www.ericzhang.me/projects/cpanel-auto-backup/
 * Remove old backups on the ftp server - https://gist.github.com/Xeroday/6050249
 * @package livesource base project
 * @author shea@livesource.co.nz
 **/
class cPanelBackupTask extends BuildTask
{
    
    private static $cpanel_domain;
    private static $cpanel_username;
    private static $cpanel_password;
    private static $cpanel_theme = 'x3';
    private static $cpanel_secure = false;

    private static $remote_ftp = true;
    private static $remote_ftp_server;
    private static $remote_ftp_username;
    private static $remote_ftp_password;
    private static $remote_ftp_port = 21;
    private static $remote_ftp_directory = '/';

    private static $error_email_address;

    public function run($request)
    {
        $config = $this->config();
        $port = $config->cpanel_secure ? 2083 : 2082;
        $domain = "$config->cpanel_domain:$port/frontend/$config->cpanel_theme/index.html";
        $url = $config->cpanel_secure ? 'ssl://' . $domain : $domain;
        $siteConfig = SiteConfig::current_site_config();

        // open socket
        if (!$socket = fsockopen($url, $port)) {
            if ($config->error_email_address) {
                $this->error("Failed to backup $siteConfig->Title. Failed to open socket connection.");
            }
        }

        // ftp or not?
        if ($config->remote_ftp) {
            if (!$this->testFTP()) {
                $this->error("Failed to backup $siteConfig->Title. Could not connect to remote ftp server");
                die;
            }
            $params = sprintf(
                "dest=ftp&server=%s&user=%s&pass=%s&port=%s&rdir=%s&submit=Generate Backup",
                $config->remote_ftp_server,
                $config->remote_ftp_username,
                $config->remote_ftp_password,
                $config->remote_ftp_port,
                $config->remote_ftp_directory
            );
        } else {
            $params = "submit=Generate Backup";
        }

        // send request
        $auth = base64_encode($config->cpanel_username . ':' . $config->cpanel_password);
        fputs($socket, "POST /frontend/" . $config->cpanel_theme . "/backup/dofullbackup.html?" . $params . " HTTP/1.0\r\n");
        fputs($socket, "Host: $domain\r\n");
        fputs($socket, "Authorization: Basic $auth\r\n");
        fputs($socket, "Connection: Close\r\n");
        fputs($socket, "\r\n");

        $response = fgets($socket, 13);
        $responseCode = substr($response, 9, 3);
        if ($responseCode != '200') {
            $this->error("Failed to backup $siteConfig->Title. cPanel server responded with status: $responseCode");
        } else {
            echo 'backup successful';
        }

        fclose($socket);
    }

    protected function testFTP()
    {
        $config = $this->config();
        
        if (!$ftp_conn = ftp_connect($config->remote_ftp_server)) {
            return false;
        }

        if (!$login = ftp_login($ftp_conn, $config->remote_ftp_username, $config->remote_ftp_password)) {
            return false;
        }

        return true;
    }

    protected function error($error)
    {
        if ($address = $this->config()->error_email_address) {
            $email = Email::create(null, $address, 'Backup error', $error);
            $email->send();
        }
        die($error);
    }
}
