<?php

class CustomFile extends DataExtension
{
    /**
     * Make use of OptimisedGDBackend to optimise images on upload
     **/
    public function onAfterUpload()
    {
        $this->optimiseImage();
    }

    public function optimiseImage()
    {
        if (!is_a($this->owner, 'Image')) {
            return;
        }
        
        if (Image::get_backend() == 'OptimisedGDBackend') {
            $filePath = Director::baseFolder() . "/" . $this->owner->Filename;
            $backend = Injector::inst()->createWithArgs(Image::get_backend(), array($filePath));
            if ($backend->hasImageResource()) {
                $backend->writeTo($filePath);
            }
        }
    }

    public function Lazy()
    {
        return $this->owner->renderWith('LazyImage');
    }
    

    public function onBeforeWrite()
    {
        if ($this->owner->FocusXY) {
            $coords = FocusPointField::fieldValueToSourceCoords($this->owner->FocusXY);
            if ($this->owner->FocusX != $coords[0] || $this->owner->FocusY != $coords[1]) {
                $this->owner->deleteFormattedImages();
            }
        }
    }
}
