<?php
class CustomLeftAndMain extends DataExtension
{
    
    public function init()
    {
        $helpFile = File::get()->filter('Name', 'admin-userguide.pdf')->first();
        if ($helpFile) {
            CMSMenu::remove_menu_item('Help');
            CMSMenu::add_link(
                'Help',
                "Help",
                $helpFile->Link(),
                -2,
                array("target"=>"_blank")
            );
        }
    }
}
