<?php

class HTML5NumericField extends NumericField
{

    protected $min;

    protected $max;

    public function getAttributes()
    {
        return array_merge(
            parent::getAttributes(),
            array(
                'type' => 'number',
                'min' => $this->min,
                'max' => $this->max
            )
        );
    }

    /**
     * Set the min variable
     *
     * @param int $min
     */
    public function setMin($min)
    {
        $this->min = $min;
        return $this;
    }

    /**
     * Set the man variable
     *
     * @param int $max
     */
    public function setMax($max)
    {
        $this->max = $max;
        return $this;
    }
}
