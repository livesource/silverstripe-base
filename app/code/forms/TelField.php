<?php

class TelField extends HTML5NumericField
{

    public function getAttributes()
    {
        return array_merge(
            parent::getAttributes(),
            array(
                'type' => 'tel'
            )
        );
    }
}
