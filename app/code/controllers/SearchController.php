<?php
class SearchController extends Page_Controller
{
    
    private static $allowed_actions = array(
        'index',
        'SearchForm',
        'results'
    );

    public function init()
    {
        parent::init();
    }
    
    public function index($request)
    {
        return $this->redirect('/');
    }
    
    public function results($data, $form, $request)
    {
        $query = $form->getSearchQuery();
        $data = array(
            'Results' => $form->getResults(),
            'Query' => $query,
            'Title' => 'Search results for "' . $query . '"'
        );

        return $this->owner->customise($data)->renderWith(array('Page_results', 'Page'));
    }

    public function Title()
    {
        return 'Search';
    }
    
    public function MetaTitle()
    {
        return $this->Title();
    }
}
