<?php

class RSSController extends ContentController
{

    const URLSegment = 'rss';

    private static $page_types = array(
        'NewsArticle',
    );
    
    /**
     * Get the rss feed entries
     */
    public function index()
    {
        $title = $this->SiteConfig()->Title . ' RSS Feed';
        $entries = NewsArticle::get()
            ->sort('PublishDate DESC')
            ->limit('20');

        if ($entries) {
            $rss = RSSFeed::create($entries, $this->Link(), $title, "", "Title", 'Content', "Author");
            return $rss->outputToBrowser();
        }
    }

    /*
     * The URL accessable link to this RSS feed
     */
    public function Link($action = null)
    {
        return Director::absoluteBaseURL() . '/rss/' . $action;
    }
}
