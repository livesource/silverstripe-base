<?php
/**
 * BetterRequirements
 *
 * This class makes 2 things possible.
 * 1) Require minified or non minified version of file based on development environment
 * 2) Allows css/js file contents to be pulled into the page, saving on extra requests where appropriate
 *
 * @package ian-potter
 * @author shea@livesource.co.nz
 **/
class BetterRequirements extends Object implements TemplateGlobalProvider
{

    /**
     * get_template_global_variables
     * @return array
     */
    public static function get_template_global_variables()
    {
        return array(
            'RequireCSS' => 'require_css',
            'RequireJS' => 'require_js',
            'InpageCSS' => 'inpage_css',
            'InpageJS' => 'inpage_js',
            'TemplatedCSS' => 'templated_css',
        );
    }


    /**
     * Require css file
     * @param string $filepath
     * @param bool|string $addMinLive - prepend .min to file extension if not in dev mode
     */
    public static function require_css($filepath, $addMinLive = true)
    {
        Requirements::css(self::process_filepath($filepath, $addMinLive));
    }


    /**
     * Require js file
     * @param string $filepath
     * @param bool|string $addMinLive - prepend .min to file extension if not in dev mode
     */
    public static function require_js($filepath, $addMinLive = true)
    {
        Requirements::javascript(self::process_filepath($filepath, $addMinLive));
    }


    /**
     * Require a custom script from a file
     * @param string $filepath
     * @param bool $addMinLive - prepend .min to file extension if not in dev mode
     */
    public static function inpage_js($filepath, $addMinLive = true)
    {
        $filepath = self::process_filepath($filepath, $addMinLive);
        $filepath = BASE_PATH . '/' . $filepath;
        if (file_exists($filepath)) {
            Requirements::customScript(file_get_contents($filepath), $filepath);
        }
    }


    /**
     * Require custom css from a file
     * @param string $filepath
     * @param bool $addMinLive - prepend .min to file extension if not in dev mode
     */
    public static function inpage_css($filepath, $addMinLive = true)
    {
        $filepath = self::process_filepath($filepath, $addMinLive);
        $filepath = BASE_PATH . '/' . $filepath;
        if (file_exists($filepath)) {
            Requirements::customCSS(file_get_contents($filepath), $filepath);
        }
    }


    /**
     * Modify a filepath to contain .min or other string postfixed to minified files
     * @param string $filepath
     * @param bool|string $addMinLive - prepend .min to file extension
     */
    public static function process_filepath($filepath, $addMinLive = true)
    {
        $themeDir = SSViewer::get_theme_folder();
        $filepath = str_replace('$ThemeDir', $themeDir, $filepath);
        if (!Director::isDev() && ($addMinLive && $addMinLive !== 'false')) {
            $addMinLive = $addMinLive === true ? '.min' : $addMinLive;
            $filepath = str_replace('.css', $addMinLive . '.css', $filepath);
            $filepath = str_replace('.js', $addMinLive . '.js', $filepath);
        }

        return $filepath;
    }


    /**
     * Render a .ss template into Requirements::customCSS
     * @param string $template - the name of the template to render with
     * @param string $contextClass - the ClassName of the object to render with
     * @param int $contextID - the ID of the object to render with
     */
    public static function templated_css($template, $contextClass, $contextID)
    {
        if ($obj = DataObject::get($contextClass)->byID($contextID)) {
            $originalComments = Config::inst()->get('SSViewer', 'source_file_comments');
            Config::inst()->update('SSViewer', 'source_file_comments', false);
            Requirements::customCSS($obj->renderWith($template));
            Config::inst()->update('SSViewer', 'source_file_comments', $originalComments);
        }
    }
}
