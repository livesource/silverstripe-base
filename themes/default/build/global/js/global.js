window.onunload = function(){
	// required for script to run again after firefox/safari history button navigation
};

var app = app || {};

// breakpoints
app.breakpoints = {
	'xxlarge': 1361,
  	'xlarge': 1136,
	'large': 946,
	'medium': 769,
	'small': 641,
	'xsmall': 451,
	'xxsmall': 0
}

// add resized event to window - example usage
// $(window).on('resized', function(){
//     	dosomething();
// }); 
var windowResizeTime;
window.onresize = function(){
    clearTimeout(windowResizeTime);
    windowResizeTime = setTimeout(function(){
    	$(window).trigger('resized');
    }, 100);
};

$(function() {
	// load modernizr and any polyfills
	var modernizrLoaded = setInterval(function() {
		if (typeof Modernizr !== 'undefined') {
			clearInterval(modernizrLoaded);

			// insert polyfills here
			
	  	}
	}, 50);
});	
