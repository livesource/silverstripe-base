function inIframe () {
    try {
        return window.self !== window.top;
    } catch (e) {
        return true;
    }
}

$(function() {
	if(!inIframe()){
		if($('body').hasClass('stage_stage')){
			var link = document.URL + '?stage=Live';
			$('body').append(
				$('<div>')
					.addClass('stage_warning')
					.html('You are currently viewing the Staging (Draft) version of this page. <a href="' + link + '">View live version</a>')
			);
		}	
	}
});
	
