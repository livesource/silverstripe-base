$(function(){
	$("img.lazy").lazyload({
    	effect : "fadeIn",
    	threshold : 50
  	});

	$("div.lazy img").lazyload({
    	effect : "fadeIn",
    	threshold : 50
  	});

  	$(window).load(function(){
	     $("html,body").trigger("scroll");
	});
});

