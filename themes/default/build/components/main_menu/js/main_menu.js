;(function ($, window) {	
	$(function(){

		var menu = $('.main_menu'),
			menu_mobile = null;

		menu.slicknav({
			allowParentLinks: true,
			init: function(){
				menu_mobile = $('.slicknav_menu');
			}
		});
		
	});
})(jQuery, window);