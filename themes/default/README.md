## Default Theme

Theme uses gruntjs.com task runner to watch, compile, minify and concatenate sass and javascript files... bower.io to manage third-party plugins and dependencies... and bourbon.io to make sass more awesome.

### Setting up

1. If you don't already have Node Package Manager installed, start by installing that https://nodejs.org/
2. Then in your command line:

```
cd <project>/themes/default
npm install
npm install -g grunt-cli
npm install -g bower
bower install
grunt
```

You should now be able to add/edit sass/js files and see the results of the grunt tasks in the command line output.

### Folder Structure

#### build/
The build folder contains the custom javascript and sass files for the application. Depending on which subfolder the files are in, they are compiled differently, see below...

#### build/global/
Contains javascript and sass files that make up the "foundation" of the application's frontend requirements ie. css resets, typography, global css and js, scss mixins and variables. The compilable files here are compiled into the css/app.css, js/app.js files.

#### build/components/
Contains javscript and sass files that belong to reusable "Components" of the application. Each component folder ie. components/main_menu/ can contain a css and js folder, containing .scss and js files. These files will also be compiled into the css/app.css, js/app.js file.

#### build/solo/
Contains JS and sass files that are compiled into their own separate files, rather than into the main app.css, app.js files. This is useful for css/js that is only required in certain sections of the application, or only on particular page types.

Example:

```build/solo/css/homepage.scss``` will compile to ```css/homepage.css```

```build/solo/js/homepage.js``` will compile to ```js/homepage.js```

#### css/
.css files compiled from build are placed here

#### js/
.js files compiled from build are placed here

#### vendor/
Any thirdparty plugins go here ie carousels etc. Use bower to install these when possible. These files can be concatenated into app.css and app.js by adding them to the vendor_js and vendor_css arrays in Gruntfile.js.

### Minification

All css and javascript compiled by the build process will be rendered in both uncompressed and compressed versions of the file. Compressed versions have the same file name but with .min prefixed to the extension ie. app.min.js.

### Gotchas

* If you create a new folder, say for a new "menu" component (build/components/menu/css), you'll need to restart the grunt process from your command line to register the new folders.

### CSS Naming Conventions and Methodology

We aim to write maintainable, modular css and javascript by defining and namespacing "components" and using consistent formatting and casing styles.

#### General Rules

* Always use css class selectors rather than id selectors
* Classnames are to be written in snakeCase
* Namespace components by referring to the top level element's classname in all css and javascript selectors. This avoids leaky css clashes
* Use the same name for a component's top level class attribute, folder and filename.
* "Subclass" components by using modifier classnames to make them reusable
* Generally format css code in the spirit of http://codeguide.co/.

#### Examples

```<a class="btn">Click Here<icon class="icon icon--arrow"></icon></a>```

The above btn element has a base class of btn. We will follow the folder and file naming convention by creating a the following file structure

```
build/components/btn/
- css/
-- btn.scss
- js/
-- btn.js
```
This way it is very obvious to a developer where the css/js for the a.btn element can be found.

btn.scss might have the following content

```css
@import "global/css/base";

.btn {
	color: black;
	background-color: grey;

	.icon {
		background-image: url(path/to/some/icon.png)
	}
}
```

In the above example, any child elements on .btn are targeted by css selectors name-spaced by .btn. This strategy should be followed to reduce the risk of leaky css clashes on large projects. Also note - ```@import "global/css/base"```; This is required at the top of all scss files, it gives you access to all variables and mixins.

Modifier classes can be applied to a component to effectively subclass it (in OOP speak). The convention for this is .componentName--modifierName

```css
.btn--orange {
	background-color: orange;
	color: white;

	.icon {
		background-image: url(path/to/some/icon-orange.png)
	}
}
```

### CSS Breakpoints and Media Queries

CSS breakpoints are defined in ```build/global/_vars.scss``` They are designed to be used in standard css media query syntax and preferably with a mobile first approach, where the base css is written for the smallest screen, then media queries are used in order of smallest - largest.

```css
.column {
	width: 100%;
}

@media (min-width: $bp_large) {
	.column {
		width: 50%;
		float: left;
	}
}

@media (min-width: $bp_large) {
	.column {
		width: 25%;
	}
}
```

In cases where you need to apply a media query that targets a max width, just make sure you -1 from the breakpoint width

```css
@media (max-width: $bp_large - 1) {
	.column {
		width: 25%;
	}
}
```

### Breakpoints in javascript

The same breakpoints that are defined in the css vars are also defined in javascript, in build/global/js/global.js. These can be accessed in javascript as needed via the global object app.breakpoints.

### Loading css and javascript requirements in SilverStripe

We've added some custom methods to SilverStripe to make adding requirements better. See app/code/extensions/BetterRequirements.php.

In general, all css and javascript requirements calls should be made in template files, keeping frontend code in the frontend.

#### Example Syntax:

```
$RequireJS("$ThemeDir/js/app.js")
$RequireCSS("$ThemeDir/css/app.css")
```

What makes these calls "better" is that if we are running our SilverStripe site dev mode, the BetterRequirements code will require the uncompressed version of the files to make debugging easier.

```
$InpageJS("$ThemeDir/js/mycomponent.js")
$InpageCSS("$ThemeDir/css/mycomponent.css")
```

$InpageJS and $InpageCSS can be used in the same way except the contents of the files they request will be rendered directly inside ```<script>``` or ```<style>``` tags in the page. This allows us to load small page-specific css/js only on the pages they are actually used on, with out adding additional http requests.
