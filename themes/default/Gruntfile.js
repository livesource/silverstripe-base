module.exports = function(grunt) {

	//	COMMANDS

	// comple css and js - $ grunt
	// optim -  run $ grunt imgoptim

	//	FILE STRUCTURE

	//	css/app.css, css/app.min.css
	//		build/global/css/*.scss
	//		build/global/css/**/*.scss
	//		build/components/componentname/css/*.scss

	//	css/singlefile.css, css/singlefile.min.css
	//		build/global/solo/css/*.scss

	//	js/app.js, js/app.min.css
	//		[vendor_js]
	//		build/global/js/*.js
	//		build/global/js/**/*.js
	//		build/components/componentname/js/*.js

	//	singlefile.js, singlefile.min.js
	//		build/global/solo/js/*.js

	var vendor_js = [
		'vendor/jquery/dist/jquery.min.js'
	];

	var vendor_css = [
	];

	var watch = {};
	var uglify = {};
	var concat = {};
	var sass = {};
	var cssmin = {};

	var js_files = [
		'build/global/js/global.js',
		'build/global/js/global.js',
		'build/components/**/js/*.js'
	];

	// WATCH =========================================/

	watch.solocss = {
		files: 'build/solo/css/*.scss',
		tasks: [
			'newer:sass:solo',
			'reload'
		]
	};

	watch.appcss = {
		files: [
			'build/global/css/mixins/*.scss',
			'build/global/css/*.scss',
			'build/components/**/*.scss'
		],
		tasks: [
			'sass:app',
			'concat:appcss',
			'sass:solo', // run solo here too incase there are changes to sass vars etc
			'reload'
		]
	};

	watch.cssmin = {
		files: ['css/*.css', '!css/*.min.css'],
		tasks: [
			'newer:cssmin:compress',
		]
	};

	watch.solojs = {
		files: 'build/solo/js/*.js',
		tasks: [
			'newer:uglify:solojsdev',
			'reload',
			'newer:uglify:solojsprod',
		]
	};

	watch.globaljs = {
		files: ['build/global/js/*.js', 'build/global/js/**/*.js', 'build/components/**/*.js'],
		tasks: [
			'concat:buildjs',
			'uglify:buildjs',
			'concat:appjs',
			'reload'
		]
	};

	// CSS COMPLILATION ====================================/

	//solo files, compiled to /css/file.css
	sass.solo = {
		options:{
			includePaths: require('node-bourbon').with('build'),
			sourceComments: true,
  			style: 'expanded' // compile in expanded format with comments for dev
  		},
	    files: [{
	      expand: true,
	      src: watch.solocss.files, // where it's coming from
	      dest: 'css', // where it's going
	      flatten: true, // removes folder tree
	      ext: '.css', // what file extension to give it - standard
	      extDot: 'last'
	    }]
	};

	// app scss files, compiled individually to /css/build/...
	sass.app = {
		options:{
			includePaths: require('node-bourbon').with('build'),
	  		style: 'expanded', // compile in expanded format with comments for dev
	  		sourceComments: 'normal', line_numbers: true, line_comments: true, debug_info: true, source_comments: true // none of these work :(
	  	},
	    files: [{
	    	//cwd: 'build',
	      expand: true,
	      src: watch.appcss.files, // where it's coming from
	      dest: 'css', // where it's going
	      ext: '.css' // what file extension to give it - standard
	    }]
	};

	// concat app.css files
	concat.appcss = {
		src: vendor_css.concat([
			'css/build/global/css/**/*.css',
			'css/build/global/css/*.css',
			'css/build/components/**/*.css'
		]),
		dest: 'css/app.css',
		seperator: '\n'
	};

	// create compressed version of all css files for prod
	cssmin.compress = {
    files: [{
      expand: true,
      cwd: 'css/',
      src: ['*.css', '!*.min.css', '!editor.css'],
      dest: 'css/',
      ext: '.min.css',
      extDot: 'last'
    }]
	}

	// JAVASCRIPT COMPLILATION ============================/

	// concat vendor.js files
	concat.vendorjs = {
		src: vendor_js,
		dest: 'js/vendor.js',
		separator: ';'
	};

	// minify vendor.js
	uglify.vendorjs = {
		files: {
			'js/vendor.min.js': [
				'js/vendor.js'
			]
		}
	};

	// concat app.js files
	concat.buildjs = {
		src: js_files,
		dest: 'js/build.js',
		separator: ';'
	};

	// minify app.js
	uglify.buildjs = {
		files: {
			'js/build.min.js': [
				'js/build.js'
			]
		}
	};

	// concat all js
	concat.appjs = {
		files: {
			'js/app.js': [
				'js/vendor.js',
				'js/build.js'
			],
			'js/app.min.js': [
				'js/vendor.min.js',
				'js/build.min.js'
			]
		}
	};

	// uglify solo files for dev
	uglify.solojsdev = {
		options: {
	    	//compress: false,
	    	mangle: false,
	    	beautify: true
	    },
	    files: [{
	        expand: true,
	        flatten: true, // removes folder tree
	        src: watch.solojs.files,
	        dest: 'js/'
	    }]
	};

	// uglify + minify solo files for prod
	uglify.solojsprod = {
	    options: {
	    	compress: {}
	    },
	    files: [{
	        expand: true,
	        flatten: true, // removes folder tree
	        src: watch.solojs.files,
	        dest: 'js/',
	        ext: '.min.js',
	        extDot: 'last',
	    }]
	};


	// IMAGE OPTIMISATION ==========================/

	imageoptim = {
	  optimise: {
	    src: ['images']
	  }
	}

	// GRUNT ======================================/

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
	 	watch: watch,
	 	sass: sass,
		concat: concat,
		cssmin: cssmin,
		uglify: uglify,
		imageoptim: imageoptim
	});

	// DEPENDENT PLUGINS =========================/

	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-sass');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-imageoptim');
	grunt.loadNpmTasks('grunt-newer');

	// TASKS =====================================/

 	grunt.registerTask('default', [
 		'sass',
 		'concat',
 		'newer:cssmin',
 		'newer:uglify',
 		'watch'
 	]);

 	grunt.registerTask("reload", "reload Chrome on OS X", function() {
		require("child_process").exec("osascript " +
		    "-e 'tell application \"Google Chrome\" " +
		      "to tell the active tab of its first window' " +
		    "-e 'reload' " +
		    "-e 'end tell'");
	});

 	grunt.registerTask('imgoptim', [
 		'imageoptim'
 	]);
}
